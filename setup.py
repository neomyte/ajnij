from setuptools import setup, find_packages

setup(
    name='ajnij',
    version='0.1.2',
    description='Jinja template reverser',
    url='https://gitlab.com/neomyte/ajnij',
    author='Emmanuel Pluot',
    author_email='emmanuel.pluot@gmail.com',
    license='N/A',
    packages=find_packages(),
    zip_safe=False
)