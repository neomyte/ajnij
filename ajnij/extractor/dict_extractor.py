import re


"""
{% for <key_name>, <value_name> in <dict_name>.items() %}<content>{% endfor %}
"""


before_loop_name_regex = r'{%\s*for\s+(?P<key_name>\w+)\s*,\s*(?P<value_name>\w+)\s+in\s+'
after_loop_name_regex = r'(?:\\\.items\\\(\\\))?\s+%}\n?(?P<content>.*?)\n?{%\s*endfor\s*%}'
dict_for_loop_regex = before_loop_name_regex + r'(?P<dict_name>\w+)' + after_loop_name_regex

def prepare_dicts_for_loop(template):
    dict_for_loops_data = [e.groupdict() for e in re.finditer(dict_for_loop_regex, template, re.DOTALL)]
    template = re.sub(before_loop_name_regex, '(?P<', template, 0, re.DOTALL)
    template = re.sub(after_loop_name_regex, '>.*)', template, 0, re.DOTALL)
    return template, dict_for_loops_data

def extract_values(template, input_, dict_):
    var_regex, dicts = prepare_dicts_for_loop(template)
    var_regex = re.sub(r'{{ *(.*?) *}}', r'(?P<\1>.*)', var_regex)
    data = re.search(var_regex, input_, re.DOTALL).groupdict()
    data[dict_['dict_name']] = {
        item[dict_['key_name']]: item[dict_['value_name']]
        for item in re.finditer(var_regex, input_)
    }
    return data, dicts
