import re

from .cast import try_cast, ensure_data_types
from .dict_extractor import prepare_dicts_for_loop, extract_values

ESCAPE_CHARS = ['(', ')', '?', '.']


def escape_char(tpl):
    for char in ESCAPE_CHARS:
        tpl = tpl.replace(char, f'\\{char}')
    return tpl

def add_border(tpl):
    return f'^{tpl}$'


def extract(template, input_):
    var_regex = escape_char(template)
    var_regex = add_border(var_regex)
    var_regex, dicts = prepare_dicts_for_loop(var_regex)
    var_regex = re.sub(r'{{ *(.*?) *}}', r'(?P<\1>.*?)', var_regex)
    data = ensure_data_types(re.search(var_regex, input_, re.DOTALL|re.MULTILINE).groupdict())
    return ensure_dict_data(data, dicts)


def ensure_dict_data(data, dicts):
    for dict_ in dicts:
        ndata, ndicts = extract_values(dict_['content'], data[dict_['dict_name']], dict_)
        ndata = ensure_dict_data(ensure_data_types(ndata), ndicts)
        for key, value in ndata.items():
            if key == dict_['dict_name'] or key not in data:
                data[key] = value
    return data