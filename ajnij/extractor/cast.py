import re

string_regex = r'^(\'[^\']*\')|("[^"]*")$'


def try_cast(data):
    if isinstance(data, dict):
        return ensure_data_types(data)
    if re.match(string_regex, data):
        return data[1:-1]
    if data.lower() in ['true', 'yes']:
        return True
    if data.lower() in ['false', 'no']:
        return False
    if data.lower() in ['null', 'nil', 'none']:
        return None

    try:
        return float(data)
    except ValueError as _:
        return data


def ensure_data_types(data):
    return { key: try_cast(val) for key, val in data.items() }