# Ajnij

Ajnij is a library designed to use jinja2 syntax as a reverse template to extract data from strings

# To run tests

First make sure all deps are installed (it is advised to create a virtual environment) using the following command:
> pip install -r requirements.txt

Then you can run the following command:
> pytest -v && pylint **/*.py

# Contributors

 * Emmanuel Pluot (aka. Neomyte)
 * Loïc Perrin (aka. Ciolnirrep)